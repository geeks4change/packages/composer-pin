# Composer Pin

This package provides bash commands:

* composer-pin (TODO)
* composer-pin-off (TODO)
* composer-pin-apply

## Why pinning?

Pinning is the practice of preventing composer updates by explicitly requiring a specific version.
It comes in two flavors:

* Version pinning
* Hash pinning

## What is version pinning?

Instead of `require foo/bar:^1.0` (update to any 1.x version), you do `composer require foo/bar:1.3`.

THis package helps with that practice. 

## What is hash pinning?

In composer, you can `composer require foo/bar#123456` to pin to a specific commit.
This is an ugly hack that is *explicitly unsupported* by composer maintainers.

That said, this package helps with that unholy practice, too.

## How to use composer-pin

**This is not yet implemented** 

In your composer-root, do
`./vendor/bin/composer-pin foo/bar`
which will 
Version pinning will effectively result in
`composer require foo/bar:1.3`
(if 1.3 is the current version).
Hash pinning will effectively result in
`composer require foo/bar:1.x-dev#123456`
(if `123456` is the current commit hash).

If `foo/bar` is not installed, nothing is done.

In any case, it is good practice to document why the pinning was done in the first place,
and work upstream for a stable release that removes the need for pinning.

## How to use composer-pin-off

**This is not yet implemented** 

In your composer-root, do
`./vendor/bin/composer-pin-off foo/bar`
which will 
Version pinning will effectively result in
`composer require foo/bar:^1`
(if on any 1.x version).
Which may result in an update.

## How to use composer-pin-apply

In your composer root, do e.g.:
`composer show -i drupal/* >pinnings/stable`.
Later you can do:
`./vendor/bin/composer-pin-apply pinnings/stable`,
which will `composer require` the exact versions you dumped above.
